# ETD Menu Tabs 

Pré-requis :
* TailwindCSS
* Alpine JS 

Modifications code :
* Ajouter ```{% include 'partials/menu.html.twig' ignore missing %}``` à l'endroit où vous souhaitez faire apparaitre le menu
* Ajouter les classes : relative overflow-x-hidden dans un wrapper de site (juste en dessous du <body>)
* Le logo utilise la variable logo_header stockées dans images/logo_header/ définie dans le thème dossier images/logo_header. Il faut donc le configurer en conséquence  
